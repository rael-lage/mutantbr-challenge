class Address {
    
    constructor(street, suite, city, zipcode, geo) {
        this._street = street;
        this._suite = suite;
        this._city = city;
        this._zipcode = zipcode;
        this._geo = geo;
    }

    getSuite() {
        return this._suite;
    }
 
} 

module.exports = Address;