class Company {
    
    constructor(name, catchPhrase, bs) {
        this._name= name;
        this._catchPhrase = catchPhrase;
        this._bs = bs;
    }

    get name() {
        return this._name;
    }
    set name(name) {
        this._name = name;
    }

    get catchPhrase() {
        return this._catchPhrase;
      }
    set catchPhrase(catchPhrase) {
        this._catchPhrase = catchPhrase;
    }

    get bs() {
        return this._bs;
      }
    set bs(bs) {
        this._bs = bs;
    }

}

module.exports = Company;