class UsersList {
    
    constructor(name) {
        this._name= name;
        this._list = [];
    }

    get nome () { return this._nome };
    set nome (value) { this._nome = value };

    addUser(user) {
        this._list.push(user);
    }

    getUsersList() {
        console.log(this._list);
    }

    getUsersWebsite() {
        const websites = this._list.map(user => user.getWebsite());
        return websites;
    }

    getUsersInfo() {
        const userInfo = this._list.map((user) => ({ 'name': user.getName(), 'email': user.getEmail(), 'company': user.getCompany() }));
        return userInfo
    }

    orderByName(users) {
        const ordered = users.sort((a, b) => (a.name > b.name) ? 1 : -1);
        return ordered;
    }

    getUsersInSuite() {
        const addressSuite = this._list.filter( user => user.getAddress().getSuite().toLowerCase().includes('suite'));
        return addressSuite;
    }
 
} 

module.exports = UsersList