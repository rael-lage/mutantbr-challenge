class User {
    
    constructor(id, name, username, email, address, phone, website, company) {
        this._id = id;
        this._name = name;
        this._username = username;
        this._email = email;
        this._address = address;
        this._phone = phone;
        this._website = website;
        this._company = company;
    }

    getName() {
        return this._name;
    }

    getEmail() {
        return this._email;
    }

    getWebsite() {
        return this._website;
    }

    getCompany() {
        return this._company;
    }

    getAddress() {
        return this._address;
    }
     
} 

module.exports = User