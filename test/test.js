const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

const app = require('../app');

chai.use(chaiHttp);

describe('API', () => {

    describe('GET /', () => {
        it('should return 200 on base route', (done) => {
          chai.request(app)
              .get('/')
              .end((err, res) => {
                    res.should.have.status(200);
                done();
              });
        });
    });

    describe('GET /usersWebsite', () => {
        it('should return a list of users websites', (done) => {
          chai.request(app)
              .get('/')
              .end((err, res) => {
                    res.should.have.status(200);
                done();
              });
        });
    });

    describe('GET /getUsersInfo', () => {
        it('should return users name, email and company', (done) => {
          chai.request(app)
              .get('/')
              .end((err, res) => {     
                    res.should.have.status(200);
                done();
              });
        });
    });

    describe('GET /getUsersInSuite', () => {
        it('should return users that have suite in adress', (done) => {
          chai.request(app)
              .get('/')
              .end((err, res) => {
                    res.should.have.status(200);
                done();
              });
        });
    });

});
