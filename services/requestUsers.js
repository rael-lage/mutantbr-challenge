const axios = require('axios');
const express = require('express');
const router = express.Router();

const User = require('../models/user');
const Company = require('../models/company');
const Geo = require('../models/geo');
const Address = require('../models/address');
const UsersList = require('../models/userList');

const path = '/users';

module.exports.requestUsers = function (url) {

    const getData = async () => {
        try {
            return await axios.get(url+path)
        } catch (error) {
            console.error(error);
        }
    }
        
    const getUsers = async () => {
        try {
            const response = await getData();
            if (response) {
                const users = response.data;
                let objUserList = new UsersList('List1');

                users.map(user => {
                    const objCompany = new Company(user.company.name, user.company.catchPhrase, user.company.bs);
                    const objGeo = new Geo(user.address.geo.lat, user.address.geo.lng);
                    const objAdress = new Address(user.address.street, user.address.suite, user.address.city, user.address.zipcode, objGeo);
                    const objUser = new User(user.id, user.name, user.username, user.email, objAdress, user.phone, user.website, objCompany)
                    objUserList.addUser(objUser);
                });

                router.get("/usersWebsite", (req, res) => {
                    res.json(objUserList.getUsersWebsite());
                });
            
                router.get("/getUsersInfo", (req, res) => {
                    res.json(objUserList.orderByName(objUserList.getUsersInfo()));
                });
            
                router.get("/getUsersInSuite", (req, res) => {
                    res.json(objUserList.getUsersInSuite());
                });
                /*
                console.log(objUserList.getUsersWebsite());
                console.log(objUserList.orderByName(objUserList.getUsersInfo()));
                console.log(objUserList.getUsersInSuite());
                */
            }
        }   catch (error) {
            console.error(error);
        }
    }
        
    return getUsers()

}

module.exports.routes = router;