'use strict';

const express = require('express');
const app = express();

const mapper = require('./services/requestUsers');

const url = 'https://jsonplaceholder.typicode.com';

mapper.requestUsers(url);

app.use('/', mapper.routes);

const PORT = 8080;
const HOST = '0.0.0.0';

app.get('/', (req, res) => {
    res.set('Content-Type', 'text/html');
    res.send(
        Buffer.from(`<h2>Links</h2>
                    <ol>
                        <li><a href="/usersWebsite">Os websites de todos os usuários</a></li>
                        <li><a href="/getUsersInfo">O Nome, email e a empresa em que trabalha (em ordem alfabética)</a></li>
                        <li><a href="/getUsersInSuite">Mostrar todos os usuários que no endereço contem a palavra "suite"</a></li>
                    </ol>`)
  );
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

module.exports = app;